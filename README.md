# POC
Proyecto realizado con redux, redux-sagas y NextJS.
El proyecto consume la API users de JSONPlaceholder y los renderiza en el home.
- Puedes realizar búsquedas con los datos renderizados.
- Tienes la posibilidad de eliminar los usuarios.

## Getting Started
Para iniciar el proyecto ejecute los siguientes comandos dependiendo del gestor de paquetes de NodeJS. 
```bash
npm run dev
# or
yarn dev
```

## Testing with Jest
```bash
npm run test
# or
yarn test
```
Los archivos de testing se encuentran en la carpeta __test__

Si deseas ejecutar todos los testings cuando se abra el cli de Jest elige la tecla <kbd>A</kbd>


## Coverage with Jest
```bash
npm run coverage
# or
yarn coverage
```

Preview
![Console](images-repo/coverage_example.png "Coverage")

