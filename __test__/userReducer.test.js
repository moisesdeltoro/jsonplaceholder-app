import { UserReducer } from "../redux/reducers/user-reducer";

describe("reducer user", () => {
    test('should return the initial state', () => {
        expect(UserReducer(undefined, {})).toEqual(
          {
            loading: false,
            users: [],
            usersFiltered: []
          }
        )
      })
});
