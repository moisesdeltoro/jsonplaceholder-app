import { all } from 'redux-saga/effects'

// Sagas
import userSaga from './userSagas'

// Export the root saga
export default function* rootSaga() {	
	yield all([userSaga()])
}