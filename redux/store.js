import { createStore , applyMiddleware} from 'redux'
import {UserReducer} from './reducers/user-reducer'

import createSagaMiddleware from 'redux-saga'
import { composeWithDevTools } from 'redux-devtools-extension'

import rootSaga from './sagas'

const sagaMiddleware = createSagaMiddleware()

const store = createStore(UserReducer,
	composeWithDevTools(applyMiddleware(sagaMiddleware)))


sagaMiddleware.run(rootSaga)
export default store