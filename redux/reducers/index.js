// combineReducers come from redux that used for combining reducers that we just made.
import { combineReducers } from 'redux'

// Reducers
import {UserReducer} from './user-reducer'

export default combineReducers({
	UserReducer,
	// Here you can registering another reducers.
})