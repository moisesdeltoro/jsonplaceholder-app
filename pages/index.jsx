import { Layout } from "../components/Layout";
import React, { useEffect } from "react";
import { GET_USERS_REQUEST } from "../redux/actions/user-actions";
import { useSelector, useDispatch } from "react-redux";
import Container from "@material-ui/core/Container";
import { CardUser } from "../components/CardUser/index";
import { Loading } from "../components/Loading";
import { Search } from "../components/Search/index";

import { makeStyles } from "@material-ui/core/styles";

export default function Home() {
  const usersFiltered = useSelector((state) => state.usersFiltered);

  const loading = useSelector((state) => state.loading);
  const dispatch = useDispatch();

  const useStyles = makeStyles({
    containerCards: {
      display: "flex",
      justifyContent: "space-around",
      width: "100%",
      flexWrap: "wrap",
    },
  });

  const classes = useStyles();

  useEffect(() => {
    dispatch({ type: GET_USERS_REQUEST });
  }, []);

  return (
    <>
      {loading && <Loading />}
      <Layout>
        <Container maxWidth="lg">
            <Search />
          <div className={classes.containerCards}>
            {usersFiltered.length !== 0 ? (
              usersFiltered.map((user) => {
                return (
                  <React.Fragment key={user.id}>
                    <CardUser user={user} />
                  </React.Fragment>
                );
              })
            ) : (
              <p>Users not found</p>
            )}
          </div>
        </Container>
      </Layout>
    </>
  );
}
