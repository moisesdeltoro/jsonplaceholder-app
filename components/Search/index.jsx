import { InputAdornment, TextField } from "@material-ui/core";
import SearchIcon from "@material-ui/icons/Search";
import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { FILTER_USERS } from "../../redux/actions/user-actions";

export const Search = () => {
  const users = useSelector((state) => state.users);
  const [searchValue, setSearchValue] = useState("");
  const dispatch = useDispatch();
  const onChangeSearch = (e) => {
    const search = e.target.value;
    setSearchValue(search);
    const usersSearch = users.filter((user) => user.name.includes(search));
    dispatch({ type: FILTER_USERS, payload: usersSearch });
  };

  return (
    <div>
      <TextField
        id="standard-basic"
        label="Search"
        InputProps={{
          startAdornment: (
            <InputAdornment position="end">
              <SearchIcon />
            </InputAdornment>
          ),
        }}
        style={{width:'100%', marginTop: 20}}
        value={searchValue}
        onChange={onChangeSearch}
      />
    </div>
  );
};
