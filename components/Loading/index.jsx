import CircularProgress from '@material-ui/core/CircularProgress';
import {useStyles} from './styles.js'

export const Loading = () =>  {
  const classes = useStyles();
  return (
    <div className={classes.loadingContainer}>
      <CircularProgress/>
      <span>Loading...</span>
    </div>
  );
}