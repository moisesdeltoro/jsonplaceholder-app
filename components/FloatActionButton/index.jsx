import AddIcon from "@material-ui/icons/Add";
import Fab from "@material-ui/core/Fab";

export const FloatActionButton = () => {
  return (
    <Fab
      color="primary"
      aria-label="add"
      style={{ position: "absolute", bottom: 40, right: 40 }}
    >
      <AddIcon />
    </Fab>
  );
};
