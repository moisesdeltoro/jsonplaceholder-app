import React from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import { Button, Typography } from "@material-ui/core";
import { DELETE_USER_REQUESTED } from "../../redux/actions/user-actions";
import { useDispatch } from "react-redux";
import {useStyles} from './styles'

export const CardUser = ({ user }) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const deleteUser = () => {
    dispatch({ type: DELETE_USER_REQUESTED, payload: user.id });
  };
  return (
    <Card className={classes.cardContainer}>
      <CardContent>
        <Typography color="textSecondary" gutterBottom>
          {user.name}
        </Typography>
      </CardContent>
      <CardActions>
        <Button size="small" color="primary" onClick={deleteUser}>
          Delete
        </Button>
      </CardActions>
    </Card>
  );
};
