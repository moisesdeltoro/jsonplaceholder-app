import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles({
    cardContainer: {
      minWidth: 275,
      marginTop: 20,
    },
  });
